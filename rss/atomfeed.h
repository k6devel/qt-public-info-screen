#ifndef ATOMFEED_H
#define ATOMFEED_H

#include "feedentry.h"
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSslError>
#include <QList>
#include <QString>
#include <QDateTime>
#include <QXmlStreamReader>
#include <QList>

class AtomFeed : public QObject
{
    Q_OBJECT
public:
    explicit AtomFeed(QNetworkAccessManager *namanager, QObject *parent = 0);

signals:
    
public slots:
    void setFeedUrl(const QUrl &url);

protected slots:
    void dataAvailable();
    void error();
    void sslError(QList<QSslError> errors);
private:
    void parseStreamSegment();
    QNetworkAccessManager *manager;
    QXmlStreamReader m_streamreader;
    QList<FeedEntry*> m_feedEntries;

    QString		m_title;
    QString		m_link;
    QString		m_tagline;
    QDateTime	m_modified;
    FeedEntry	*m_currentEntry;
    QString		m_cdatabuffer;
};

#endif // RSSFEED_H
