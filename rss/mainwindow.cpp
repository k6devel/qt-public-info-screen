#include "mainwindow.h"
#include <QQuickView>
#include <QQmlContext>
#include <QString>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    QQuickView view;
    QQmlContext *ctx = view.rootContext();
    view.setSource(QUrl("qrc:/qml/main.qml"));
    view.show();
}

MainWindow::~MainWindow()
{
}

void MainWindow::actioon()
{
    emit newUrl(QString("bla"));
}
