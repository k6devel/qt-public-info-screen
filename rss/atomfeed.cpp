#include "atomfeed.h"
#include <QDebug>

AtomFeed::AtomFeed(QNetworkAccessManager *namanager, QObject *parent) :
    QObject(parent), manager(namanager), m_currentEntry(NULL)
{
}

void AtomFeed::setFeedUrl(const QUrl &url) {
    QNetworkRequest request(url);
    QNetworkReply *reply = manager->get(request);
    connect(reply, SIGNAL(readyRead()), this, SLOT(dataAvailable()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),this,SLOT(error()));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslError(QList<QSslError>)));
}

void AtomFeed::dataAvailable() {
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    if(!reply) return;
    m_streamreader.addData(reply->readAll());
    parseStreamSegment();
    if(reply->isFinished()) reply->deleteLater();
}

void AtomFeed::error() {
    qDebug()<< "Network Error";
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    if(!reply) return;
    if(reply->isFinished()) reply->deleteLater();
}

void AtomFeed::sslError(QList<QSslError> errors) {
    qDebug()<< "SSL Error" << errors;
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    if(!reply) return;
    if(reply->isFinished()) reply->deleteLater();
}

void AtomFeed::parseStreamSegment()  {
    while(!m_streamreader.atEnd()) {
        m_streamreader.readNext();
        if(m_streamreader.isStartElement()) {
            if(m_currentEntry==NULL) {
                if(m_streamreader.name().toString().toLower()=="entry") {
                    if(m_currentEntry!=NULL) qDebug()<<"FIXME:corrupt feed leaking memory";
                    m_currentEntry = new FeedEntry();
                    m_cdatabuffer.clear();
                }
            } else if(m_streamreader.name().toString().toLower()=="link") {
                m_currentEntry->link=m_streamreader.attributes().value("href").toString();
            }
        } else if(m_streamreader.isEndElement()) {
            if(m_currentEntry==NULL) { //stream header
            } else { // we have an valid stream entry over here
                if(m_streamreader.name().toString().toLower()=="entry") {
                    m_currentEntry->print();
                    m_feedEntries.append(m_currentEntry);
                    m_currentEntry=NULL;
                } else if(m_streamreader.name().toString().toLower()=="title") {
                    m_currentEntry->title=m_cdatabuffer.trimmed();
                }  else if(m_streamreader.name().toString().toLower()=="name") {
                    m_currentEntry->author=m_cdatabuffer.trimmed();
                } else if(m_streamreader.name().toString().toLower()=="id") {
                    m_currentEntry->id=m_cdatabuffer.trimmed();
                } else if(m_streamreader.name().toString().toLower()=="content") {
                    m_currentEntry->content=m_cdatabuffer.trimmed();
                } else if(m_streamreader.name().toString().toLower()=="issued") {
                    QString date = m_cdatabuffer.trimmed(); //FIXME time is not read in correctly by fromString
                    m_currentEntry->issued = QDateTime::fromString(date,"yyyy-MM-dd'T'HH:mm:ss'Z'");
                }
            }
            m_cdatabuffer.clear();
        } else if(m_streamreader.isCharacters()) {
            m_cdatabuffer.append(m_streamreader.text());
        }
    }
}
