#ifndef FEEDENTRY_H
#define FEEDENTRY_H
#include <QString>
#include <QObject>
#include <QDateTime>

class FeedEntry:public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString author READ getAuthor WRITE setAuthor NOTIFY authorChanged)
    Q_PROPERTY(QString title READ getTitle WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QString link READ getLink WRITE setLink NOTIFY linkChanged)
    Q_PROPERTY(QString id READ getId WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString content READ getContent WRITE setContent NOTIFY contentChanged)
    Q_PROPERTY(QDateTime issued READ getIssued WRITE setIssued NOTIFY issuedChanged)

public:
    FeedEntry();
    FeedEntry(const FeedEntry &other);
    QString author;
    QString	title;
    QString link;
    QString id;
    QString content;
    QString summary;
    QDateTime issued;

    void print();
    QString getAuthor() const;
    void setAuthor(const QString &value);
    QString getTitle() const;
    void setTitle(const QString &value);
    QString getLink() const;
    void setLink(const QString &value);
    QString getId() const;
    void setId(const QString &value);
    QString getContent() const;
    void setContent(const QString &value);
    QString getSummary() const;
    void setSummary(const QString &value);
    QDateTime getIssued() const;
    void setIssued(const QDateTime &value);
signals:
    void authorChanged(QString author);
    void titleChanged(QString title);
    void linkChanged(QString link);
    void idChanged(QString id);
    void contentChanged(QString content);
    void issuedChanged(QDateTime issued);
};

#endif // FEEDENTRY_H
