//#include "mainwindow.h"

#include <QApplication>
#include <QQuickView>
#include <QQmlContext>
#include <QQmlEngine>
#include <QtQuick>
#include "unifeed.h"
#include "feedlist.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QNetworkAccessManager manager(&a);
    UniFeed uniFeed(&manager, &a);

    QQuickView view;
    qRegisterMetaType<FeedEntry*>();
    qmlRegisterType<FeedEntry>("FeedEntry",1,0,"FeedEntry");
    view.setSource(QUrl("qrc:/qml/main.qml"));
    QQmlContext *ctx = view.rootContext();
    FeedList fl(&a);
    a.connect(&uniFeed,SIGNAL(newFeedEntry(FeedEntry*)),&fl,SLOT(newFeedEntry(FeedEntry*)));
    ctx->setContextProperty("feedList",&fl);
    ctx->setContextProperty("feedGetter",&uniFeed);
    view.setWidth(500);
    view.setHeight(500);
    view.show();
    /*
    MainWindow w;
    w.connect(&w,SIGNAL(newUrl(QUrl)),&uniFeed,SLOT(setFeedUrl(QUrl)));
    w.show();
    */
    
    return a.exec();
}
