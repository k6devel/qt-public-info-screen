#ifndef FEEDLIST_H
#define FEEDLIST_H

#include <QAbstractListModel>
#include <QList>
#include "feedentry.h"

class FeedList : public QAbstractListModel
{
    Q_OBJECT
public:
    enum FeedRoles{
        entry = Qt::UserRole + 1,
        author,
        title,
        link,
        id,
        content,
        issued
    };
    explicit FeedList(QObject *parent = 0);
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;
signals:

public slots:
    void newFeedEntry(FeedEntry *fe);
private:
    QList<FeedEntry*> m_feedEntries;
};

#endif // FEEDLIST_H
