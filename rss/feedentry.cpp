#include "feedentry.h"
#include <QDebug>

FeedEntry::FeedEntry():QObject() {
}

FeedEntry::FeedEntry(const FeedEntry &other)
    :QObject()
{
    this->setAuthor(other.getAuthor());
    this->setTitle(other.getTitle());
    this->setLink(other.getLink());
    this->setId(other.getId());
    this->setContent(other.getContent());
    this->setSummary(other.getSummary());
    this->setIssued(other.getIssued());
}

QString FeedEntry::getAuthor() const
{
    return author;
}

void FeedEntry::setAuthor(const QString &value)
{
    author = value;
    emit authorChanged(value);
}

QString FeedEntry::getTitle() const
{
    return title;
}

void FeedEntry::setTitle(const QString &value)
{
    title = value;
    emit titleChanged(title);
}

QString FeedEntry::getLink() const
{
    return link;
}

void FeedEntry::setLink(const QString &value)
{
    link = value;
    emit linkChanged(link);
}

QString FeedEntry::getId() const
{
    return id;
}

void FeedEntry::setId(const QString &value)
{
    id = value;
    emit idChanged(id);
}

QString FeedEntry::getContent() const
{
    return content;
}

void FeedEntry::setContent(const QString &value)
{
    content = value;
    emit contentChanged(content);
}

QString FeedEntry::getSummary() const
{
    return summary;
}

void FeedEntry::setSummary(const QString &value)
{
    summary = value;
}

QDateTime FeedEntry::getIssued() const
{
    return issued;
}

void FeedEntry::setIssued(const QDateTime &value)
{
    issued = value;
    emit issuedChanged(issued);
}

void FeedEntry::print() {
    qDebug()<<"Title:"<<title;
    qDebug()<<" author:"<<author;
    qDebug()<<" link:"<<link;
    qDebug()<<" id:"<<id;
    qDebug()<<" summary:"<<summary;
    qDebug()<<" content:"<<content;
    qDebug()<<" issued:"<<issued;
}
