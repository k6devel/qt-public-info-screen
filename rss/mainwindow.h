#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUrl>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

signals:
    void newUrl(QUrl q);
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void actioon();
    
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
