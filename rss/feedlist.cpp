#include "feedlist.h"

FeedList::FeedList(QObject *parent) :
    QAbstractListModel(parent)
{
}

int FeedList::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_feedEntries.size();
}

QVariant FeedList::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(role);
    if(!index.isValid()) return QVariant();
    if(index.row() >= m_feedEntries.size()) return QVariant();
    return qVariantFromValue(m_feedEntries.at(index.row()));
}

QHash<int, QByteArray> FeedList::roleNames() const
{
    QHash<int, QByteArray> retval;
    retval[entry] = "entry";
    return retval;
}

void FeedList::newFeedEntry(FeedEntry *fe)
{
    int row = m_feedEntries.size();
    beginInsertRows(QModelIndex(),row,row);
    m_feedEntries.append(fe);
    endInsertRows();
}
