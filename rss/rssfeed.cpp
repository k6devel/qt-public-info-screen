#include "rssfeed.h"
#include <QDebug>

RssFeed::RssFeed(QNetworkAccessManager *namanager, QObject *parent) :
    QObject(parent), manager(namanager), m_currentEntry(NULL)
{
}

void RssFeed::setFeedUrl(const QUrl &url) {
    QNetworkRequest request(url);
    QNetworkReply *reply = manager->get(request);
    connect(reply, SIGNAL(readyRead()), this, SLOT(dataAvailable()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),this,SLOT(error()));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslError(QList<QSslError>)));
}

void RssFeed::dataAvailable() {
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    if(!reply) return;
    m_streamreader.addData(reply->readAll());
    parseStreamSegment();
    if(reply->isFinished()) reply->deleteLater();
}

void RssFeed::error() {
    qDebug()<< "Network Error";
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    if(!reply) return;
    if(reply->isFinished()) reply->deleteLater();
}

void RssFeed::sslError(QList<QSslError> errors) {
    qDebug()<< "SSL Error" << errors;
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    if(!reply) return;
    if(reply->isFinished()) reply->deleteLater();
}

void RssFeed::parseStreamSegment()  {
    while(!m_streamreader.atEnd()) {
        m_streamreader.readNext();
        if(m_streamreader.isStartElement()) {
            if(m_currentEntry==NULL) {
                if(m_streamreader.name().toString().toLower()=="item") {
                    if(m_currentEntry!=NULL) qDebug()<<"FIXME:corrupt feed leaking memory";
                    m_currentEntry = new FeedEntry();
                    m_cdatabuffer.clear();
                }
            }
        } else if(m_streamreader.isEndElement()) {
            qDebug()<<m_streamreader.name().toString();
            if(m_currentEntry==NULL) { //stream header
            } else { // we have an valid stream entry over here
                if(m_streamreader.name().toString().toLower()=="link") {
                    m_currentEntry->link = m_cdatabuffer.trimmed();
                } else if(m_streamreader.name().toString().toLower()=="item") {
                    m_currentEntry->print();
                    m_feedEntries.append(m_currentEntry);
                    m_currentEntry=NULL;
                } else if(m_streamreader.name().toString().toLower()=="title") {
                    m_currentEntry->title=m_cdatabuffer.trimmed();
                }  else if(m_streamreader.name().toString().toLower()=="creator") {
                    m_currentEntry->author=m_cdatabuffer.trimmed();
                } else if(m_streamreader.name().toString().toLower()=="guid") {
                    m_currentEntry->id=m_cdatabuffer.trimmed();
                } else if(m_streamreader.name().toString().toLower()=="description") {
                    m_currentEntry->content=m_cdatabuffer.trimmed();
                } else if(m_streamreader.name().toString().toLower()=="pubdate") {
                    QString date = m_cdatabuffer.trimmed(); // Tue, 29 Oct 2013 22:25:24 +0000
                    QLocale::setDefault(QLocale::English);
                    QDateTime dateAsQDateTime = QLocale().toDateTime(date, "ddd, dd MMM yyyy HH:mm:ss '+0000'");

                    m_currentEntry->issued = dateAsQDateTime;
                }
            }
            m_cdatabuffer.clear();
        } else if(m_streamreader.isCharacters()) {
            m_cdatabuffer.append(m_streamreader.text());
        }
    }
}
