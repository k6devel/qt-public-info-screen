#-------------------------------------------------
#
# Project created by QtCreator 2013-04-19T19:10:38
#
#-------------------------------------------------

QT       += core gui network xml qml quick
LIBS 	 += -lpng -lz -lglib-2.0
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt-gui
TEMPLATE = app

FORMS    += mainwindow.ui

HEADERS += \
    mainwindow.h \
    atomfeed.h \
    rssfeed.h \
    feedentry.h \
    unifeed.h \
    feedlist.h

SOURCES += \
    atomfeed.cpp \
    rssfeed.cpp \
    main.cpp\
    mainwindow.cpp \
    feedentry.cpp \
    unifeed.cpp \
    feedlist.cpp
OTHER_FILES += main.qml

RESOURCES += \
    qt-gui.qrc

INSTALLS = target
target.path = /usr/bin
