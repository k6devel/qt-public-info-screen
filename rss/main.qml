import QtQuick 2.0
import FeedEntry 1.0

Rectangle {
    id: page
    anchors.fill: parent

    color: "lightgray"

    TextInput {
        id: url
        text: "http://leben-in-k6.de/blog/feed/" //http://rss.golem.de/rss.php?feed=ATOM1.0"
        y: 30
        anchors.horizontalCenter: page.horizontalCenter
        font.pointSize: 12;
    }
    Rectangle {
        id:button
        x: parent.width/4
        y: 55
        width: parent.width/2
        height:30
        z:7
        MouseArea {
            anchors.fill: parent
            onClicked: feedGetter.setFeedUrl(url.getText(0,url.length))
            z:1
        }

        Text {
            anchors.fill: parent
            text: "Go"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter;

        }
        //Text: { text: "go" }
    }
    Component {
        id: feedDelegate
        Column {
            id: wrapper
            z: PathView.z
            scale: PathView.scale
            Rectangle {

                transform: Rotation {
                    origin.x: width/2;
                    origin.y: height/2;
                    axis.x:0; axis.y: 1; axis.z: 0
                    angle: PathView.angle
                }

                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                border.color: "black"

                id: delegateItem

                width: page.width/3;
                height: page.height/3;
                property var f: FeedEntry;
                Text {
                    clip: true
                    text: entry.author+" "+entry.title+" "+entry.content;
                    horizontalAlignment: Text.AlignHCenter
                    font.pointSize: 16
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    wrapMode: Text.Wrap
                    anchors.fill: parent
                }

            }
        }
    }
    /*
    ListView {
        y:100
        width: 500; height: 100
        model: feedList
        delegate: feedDelegate

    }
    */
    PathView {
        Keys.onRightPressed: if (!moving) { incrementCurrentIndex(); console.log(moving) }
                 Keys.onLeftPressed: if (!moving) decrementCurrentIndex()
             id: view
             y:100
             //width: 500; height: 500
             anchors.fill: parent

             focus: true
             model: feedList
             delegate: feedDelegate
             path: Path {
                 startX: page.width/5; startY: page.height*2/3
                 PathAttribute { name: "angle"; value: 60}
                 PathAttribute { name: "scale"; value: 0.5}
                 PathAttribute { name: "z"; value: 0}
                 PathQuad { x:page.width/2; y: page.height/3; controlX: page.width/5; controlY: page.height/3 }
                 PathAttribute { name: "angle"; value: 0}
                 PathAttribute { name: "scale"; value: 1}
                 PathAttribute { name: "z"; value: 100}
                 PathQuad { x:page.width*4/5; y: page.height*2/3; controlX: page.width*4/5; controlY: page.height/3 }
                 PathAttribute { name: "angle"; value: 60}
                 PathAttribute { name: "scale"; value: 0.5}
                 PathAttribute { name: "z"; value: 0}
             }
         }
}
