#ifndef UNIFEED_H
#define UNIFEED_H

#include "feedentry.h"
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSslError>
#include <QList>
#include <QString>
#include <QDateTime>
#include <QXmlStreamReader>
#include <QList>

class UniFeed : public QObject
{
    Q_OBJECT
public:
    explicit UniFeed(QNetworkAccessManager *namanager, QObject *parent = 0);
signals:
    void newFeedEntry(FeedEntry *fe);
public slots:
    void setFeedUrl(const QString &url);

protected slots:
    void setFeedUrl(const QUrl &url);
    void dataAvailable();
    void error();
    void sslError(QList<QSslError> errors);
private:
    void parseStreamSegment();
    QNetworkAccessManager *manager;
    QXmlStreamReader m_streamreader;

    QString		m_title;
    QString		m_link;
    QString		m_tagline;
    QDateTime	m_modified;
    FeedEntry	*m_currentEntry;
    QString		m_cdatabuffer;
};

#endif // UNIFEED_H
