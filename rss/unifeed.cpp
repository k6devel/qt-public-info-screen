#include "unifeed.h"

UniFeed::UniFeed(QNetworkAccessManager *namanager,QObject *parent) :
    QObject(parent), manager(namanager), m_currentEntry(NULL)
{
}

void UniFeed::setFeedUrl(const QString &url)
{
    setFeedUrl(QUrl(url));
}

void UniFeed::setFeedUrl(const QUrl &url) {
    QNetworkRequest request(url);
    QNetworkReply *reply = manager->get(request);
    connect(reply, SIGNAL(readyRead()), this, SLOT(dataAvailable()));
    connect(reply,SIGNAL(error(QNetworkReply::NetworkError)),this,SLOT(error()));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslError(QList<QSslError>)));
}

void UniFeed::dataAvailable() {
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    if(!reply) return;
    QByteArray a = reply->readAll();
    //qDebug()<<a;
    m_streamreader.addData(a);
    parseStreamSegment();
    if(reply->isFinished()) reply->deleteLater();
}

void UniFeed::error() {
    qDebug()<< "Network Error";
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    if(!reply) return;
    if(reply->isFinished()) reply->deleteLater();
}

void UniFeed::sslError(QList<QSslError> errors) {
    qDebug()<< "SSL Error" << errors;
    QNetworkReply *reply = static_cast<QNetworkReply*>(sender());
    if(!reply) return;
    if(reply->isFinished()) reply->deleteLater();
}

void UniFeed::parseStreamSegment()  {
    while(!m_streamreader.atEnd()) {
        m_streamreader.readNext();
        if(m_streamreader.isStartElement()) {
            if(m_currentEntry==NULL) {
                if(m_streamreader.name().toString().toLower()=="item"
                        || m_streamreader.name().toString().toLower()=="entry") {
                    if(m_currentEntry!=NULL) qDebug()<<"FIXME:corrupt feed leaking memory";
                    m_currentEntry = new FeedEntry;
                    m_cdatabuffer.clear();
                }
            } else if(m_streamreader.name().toString().toLower()=="link") {
               QXmlStreamAttributes a = m_streamreader.attributes();
               if(a.hasAttribute("href")) {
                    m_currentEntry->link=m_streamreader.attributes().value("href").toString();
               }
            }
        } else if(m_streamreader.isEndElement()) {
            qDebug()<<m_streamreader.name().toString();
            if(m_currentEntry==NULL) { //stream header
            } else { // we have an valid stream entry over here
                if(m_streamreader.name().toString().toLower()=="link") {
                    QString data = m_cdatabuffer.trimmed();
                    if(data.length()) {
                        m_currentEntry->link = data;
                    }
                } else if(m_streamreader.name().toString().toLower()=="item"
                          || m_streamreader.name().toString().toLower()=="entry") {
                    m_currentEntry->print();
                    emit newFeedEntry(m_currentEntry);
                    m_currentEntry=NULL;
                } else if(m_streamreader.name().toString().toLower()=="title") {
                    m_currentEntry->title=m_cdatabuffer.trimmed();
                }  else if(m_streamreader.name().toString().toLower()=="creator"
                           || m_streamreader.name().toString().toLower()=="name") {
                    m_currentEntry->author=m_cdatabuffer.trimmed();
                } else if(m_streamreader.name().toString().toLower()=="guid"
                          || m_streamreader.name().toString().toLower()=="id") {
                    m_currentEntry->id=m_cdatabuffer.trimmed();
                } else if(m_streamreader.name().toString().toLower()=="summary") {
                    m_currentEntry->summary = m_cdatabuffer.trimmed();
                } else if(m_streamreader.name().toString().toLower()=="description"
                          || m_streamreader.name().toString().toLower()=="content") {
                    m_currentEntry->content=m_cdatabuffer.trimmed();
                } else if(m_streamreader.name().toString().toLower()=="pubdate") {
                    QString date = m_cdatabuffer.trimmed(); //FIXME time is not read in correctly by fromString
                    // Tue, 29 Oct 2013 22:25:24 +0000
                    // m_currentEntry->issued = QDateTime::fromString(date,"ddd, d MMM yyyy HH:mm:ss '+0000'");
                    m_currentEntry->issued = QDateTime::fromString(date,Qt::RFC2822Date);
                } else if(m_streamreader.name().toString().toLower()=="issued") {
                    QString date = m_cdatabuffer.trimmed(); //FIXME time is not read in correctly by fromString
                    m_currentEntry->issued = QDateTime::fromString(date,"yyyy-MM-dd'T'HH:mm:ss'Z'");
                }
            }
            m_cdatabuffer.clear();
        } else if(m_streamreader.isCharacters()) {
            m_cdatabuffer.append(m_streamreader.text());
        }
    }
}
