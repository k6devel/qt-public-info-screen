#include "camerawindow.h"
#include <QHBoxLayout>
#include <QWidget>
#include "facestate.h"
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
using namespace cv;


//Constructor
CameraWindow::CameraWindow(QWidget *parent)
    : QWidget(parent)
{

    FaceState *f= new FaceState();
    m_cvwidget = new CameraWidget(f,this);
    QHBoxLayout *layout = new QHBoxLayout();

    layout->addWidget(m_cvwidget);
    layout->addWidget(f);
    setLayout(layout);
    resize(400, 200);

    //connect(button, SIGNAL(pressed()), this, SLOT(savePicture()));
    //connect(this, &CameraWindow::startThread, m_imageGrabber, &ImageGrabber::doWork);

    emit startThread();
    startTimer(100);  // ms timer
}


//Puts a new frame in the widget every 100 msec
void CameraWindow::timerEvent(QTimerEvent*)
{
    Mat *m = m_buf.getImagePointer(false);
    m_cvwidget->putFrame(m);
}

//Saves a new picture
/*
void CameraWindow::savePicture(void)
{
    //IplImage *image = cvQueryFrame(m_camera);

    QPixmap photo = m_cvwidget->toPixmap(image);

    if (photo.save(QString::number(m_photoCounter) + ".jpg")) {
        qDebug("Picture successfully saved!");
        m_photoCounter++;
    } else {
        qDebug("Error while saving the picture");
    }
}

*/
