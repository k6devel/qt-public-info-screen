#include "imagebuffer.h"

ImageBuffer::ImageBuffer():
    m_imageGrabber(this),
    m_images(3),
    m_detectIdx(1),
    m_grabberIdx(0),
    m_grabberThread()
{
    for(int i=0;i<3;i++) {
        m_imageGrabber.getCam() >> m_images[i];
        m_images[i].convertTo(m_images[i],CV_8UC1);
    }
    m_imageGrabber.moveToThread(&m_grabberThread);
    connect(this, SIGNAL(startThread()),&m_imageGrabber, SLOT(doWork()));
    m_grabberThread.start();
    emit startThread();
}

Mat *ImageBuffer::getImagePointer(bool grabber)
{
    //we do have a problem if the image detection is faster
    //than the image grabbing of the camera -> non-linear time!
    Mat *retval;
    m_mutex.lock();
    if(grabber) {
        m_grabberIdx++;
        if(m_grabberIdx>2) m_grabberIdx=0;
        if(m_detectIdx==m_grabberIdx) {
            m_grabberIdx++;
            if(m_grabberIdx>2) m_grabberIdx=0;
        }
        retval = &m_images[m_grabberIdx];
    } else {
        m_detectIdx++;
        if(m_detectIdx>2) m_detectIdx=0;
        if(m_detectIdx==m_grabberIdx) {
            m_detectIdx++;
            if(m_detectIdx>2) m_detectIdx=0;
        }
        retval = &m_images[m_detectIdx];
    }
    m_mutex.unlock();
    return retval;
}
