#ifndef IMAGEGRABBER_H
#define IMAGEGRABBER_H

#include <QObject>
#include "opencv2/opencv.hpp"

using namespace cv;

class ImageBuffer;
class ImageGrabber : public QObject
{
    Q_OBJECT
public:
    ImageGrabber(ImageBuffer *ib);
    VideoCapture& getCam() {return cap;}
public Q_SLOTS:
    //FIXME how does this thread terminate
    void doWork();
private:
    ImageBuffer *m_ib;
    VideoCapture cap;
};

#endif // IMAGEGRABBER_H
