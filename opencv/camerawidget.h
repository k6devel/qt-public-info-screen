#ifndef CAMERAWIDGET_H
#define CAMERAWIDGET_H

#include <QPixmap>
#include <QLabel>
#include <QWidget>
#include <QVBoxLayout>
#include <QPen>
#include <QImage>
#include <QDebug>
#include <QRect>
#include <opencv/cv.h>

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "facestate.h"

class CameraWidget : public QWidget
{

public:
    CameraWidget(FaceState *f,QWidget *parent = 0);
    ~CameraWidget(void);
    QPixmap toPixmap(IplImage *);
    void putFrame(cv::Mat *);

    QPixmap Mat2QImage(const cv::Mat &src);
public slots:
    void faceDetected(int x,int y,int width,int height);
    void noFace();
    void leftFace();
    void rightFace();
private:
    QLabel *m_imageLabel;
    QVBoxLayout *m_layout;
    QImage m_image;
    cv::CascadeClassifier face_cascade;
    cv::CascadeClassifier hand_cascade;
    cv::CascadeClassifier face_cascade3;
    cv::CascadeClassifier upper_body;
    QPen penFace;
    QPen penHand;
    QRect m_facePos;
    FaceState *m_faceState;
}; 

#endif
