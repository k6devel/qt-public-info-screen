#ifndef CAMERAWINDOW_H_
#define CAMERAWINDOW_H_

#include <QThread>
#include <QVector>
#include <QMutex>
#include <QWidget>
#include <QVBoxLayout>
#include <QDebug>
#include <QPushButton>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "camerawidget.h"
#include "imagebuffer.h"

class CameraWindow : public QWidget
{
    Q_OBJECT
    signals:
    void startThread();
public:
    CameraWindow(QWidget *parent=0);
    /// getImage returns a unused image, called from two different threads!
    /// works on m_grabberIdx,m_detectIdx in critical section
private:
    CameraWidget *m_cvwidget;

    int m_photoCounter;

    ImageBuffer m_buf;

protected:
    void timerEvent(QTimerEvent*);

public slots:
    //void savePicture(void);

};

#endif
