#ifndef FACESTATE_H
#define FACESTATE_H

#include <QWidget>
#include <QPen>
#include <QLabel>
#include <QHBoxLayout>
#include <opencv/cv.h>

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class FaceState : public QWidget
{
    Q_OBJECT
public:
    explicit FaceState(QWidget *parent = 0);
    enum State {NO_FACE,NORMAL_FACE,LEFT_FACE,RIGHT_FACE,LOST_FACE};
    void doFrame(cv::Mat &image, QPainter &p);
    void createRoi(cv::Mat &input, cv::Mat &output);
signals:
    void face(int x,int y,int sizex,int sizey);
    void noface();
    void left();
    void right();
public slots:
private:
    void rotate(cv::Mat &src, double angle, cv::Mat &dst);
    void emitDirection();
    State m_state;
    int m_lostCount;
    int m_localSearchSize;
    cv::Rect m_facePos;
    cv::CascadeClassifier face_cascade;
    float m_rotate;
    float m_minSearchScale;
};

#endif // FACESTATE_H
