#include "facestate.h"
#include <QPainter>
#include <QDebug>

using namespace cv;

FaceState::FaceState(QWidget *parent) :
    QWidget(parent),
    m_state(NO_FACE),
    m_localSearchSize(80),
    m_rotate(20.0),
    m_minSearchScale(0.7)
{
    qDebug()<<"no face";
    if(!face_cascade.load("/home/haarcascade_frontalface_alt_tree.xml")) {
        qFatal("failed to load haarcascade_frontalface_alt.xml");
    }
}

void FaceState::rotate(cv::Mat& src, double angle, cv::Mat& dst)
{
    int len = std::max(src.cols, src.rows);
    cv::Point2f pt(len/2., len/2.);
    cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);
    cv::warpAffine(src, dst, r, cv::Size(len, len));
}

void FaceState::emitDirection()
{
    if(m_state==LEFT_FACE) {
        emit left();
    } else if(m_state==RIGHT_FACE) {
        emit right();
    }
}

void FaceState::createRoi(Mat &input,Mat &output)
{
    Rect roi;
    roi.x = m_facePos.x - m_localSearchSize/2;
    roi.y = m_facePos.y - m_localSearchSize/2;
    roi.width = m_facePos.width + m_localSearchSize;
    roi.height = m_facePos.height + m_localSearchSize;
    if(roi.x<0) roi.x=0;
    if(roi.y<0) roi.y=0;
    if(roi.x+roi.width>input.cols) roi.width=input.cols-roi.x;
    if(roi.y+roi.height>input.rows) roi.height=input.rows-roi.y;
    output = input(roi);
}

void FaceState::doFrame(Mat &image,QPainter &p)
{
    std::vector<Rect> faces;
    qDebug()<<"doFrame:"<<m_state;
    switch(m_state) {
    case NO_FACE:
    {
        face_cascade.detectMultiScale( image, faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(25, 25));
        if(faces.size()>=1) {
            m_lostCount=0;
            m_facePos = faces[0];
            m_state=NORMAL_FACE;
            emit face(m_facePos.x,m_facePos.y,m_facePos.width,m_facePos.height);
            qDebug()<<"normal face";
        }
    }
        break;
    case NORMAL_FACE:
    {
        Mat localFace;
        createRoi(image,localFace);
        face_cascade.detectMultiScale( localFace, faces, 1.1, 2, CV_HAAR_SCALE_IMAGE, Size(m_facePos.width*m_minSearchScale, m_facePos.height*m_minSearchScale));
        if(faces.size()!=1) {
            Mat leftFace;
            rotate(localFace,m_rotate,leftFace);
            face_cascade.detectMultiScale( leftFace, faces, 1.1, 2, CV_HAAR_SCALE_IMAGE, Size(m_facePos.width*m_minSearchScale, m_facePos.height*m_minSearchScale));
            if(faces.size()==1) {
                m_lostCount=0;
                m_state=LEFT_FACE;
                qDebug()<<"left face";
                emitDirection();
            } else {
                Mat rightFace;
                rotate(localFace,-m_rotate,rightFace);
                face_cascade.detectMultiScale( rightFace, faces, 1.1, 2, CV_HAAR_SCALE_IMAGE, Size(m_facePos.width*m_minSearchScale, m_facePos.height*m_minSearchScale));
                if(faces.size()==1) {
                    m_lostCount=0;
                    m_state=RIGHT_FACE;
                    qDebug()<<"right face";
                    emitDirection();
                } else {
                    m_state=LOST_FACE;
                    emit noface();
                    qDebug()<<"lost face";
                }
            }
        } else {
            m_facePos.x=m_facePos.x-m_localSearchSize/2+faces[0].x;
            if(m_facePos.x<0) m_facePos.x=0;
            m_facePos.y=m_facePos.y-m_localSearchSize/2+faces[0].y;
            if(m_facePos.y<0) m_facePos.y=0;
            m_facePos.width=faces[0].width;
            m_facePos.height=faces[0].height;
            emit face(m_facePos.x,m_facePos.y,m_facePos.width,m_facePos.height);
        }

    }
        break;
    case LEFT_FACE:
    {
        Mat localFace;
        createRoi(image, localFace);
        Mat leftFace;
        rotate(localFace,m_rotate,leftFace);
        face_cascade.detectMultiScale( leftFace, faces, 1.1, 2, CV_HAAR_SCALE_IMAGE, Size(m_facePos.width*m_minSearchScale, m_facePos.height*m_minSearchScale));
        if(faces.size()!=1) {
            face_cascade.detectMultiScale( localFace, faces, 1.1, 2, CV_HAAR_SCALE_IMAGE, Size(m_facePos.width*m_minSearchScale, m_facePos.height*m_minSearchScale));
            if(faces.size()==1) {
                m_lostCount=0;
                m_state=NORMAL_FACE;
                emit face(m_facePos.x,m_facePos.y,m_facePos.width,m_facePos.height);
                qDebug()<<"normal face";
            } else {
                m_state=LOST_FACE;
                emit noface();
                qDebug()<<"lost face";
            }
        } else {
            emitDirection();
            p.drawLine(m_facePos.x+m_facePos.width,m_facePos.y,m_facePos.x+m_facePos.width,m_facePos.y+m_facePos.height);
        }
        break;
    }
    case RIGHT_FACE:
    {
        Mat localFace;
        createRoi(image, localFace);
        Mat rightFace;
        rotate(localFace,-m_rotate,rightFace);
        face_cascade.detectMultiScale( rightFace, faces, 1.1, 2, CV_HAAR_SCALE_IMAGE, Size(m_facePos.height*m_minSearchScale, m_facePos.height*m_minSearchScale));
        if(faces.size()!=1) {
            face_cascade.detectMultiScale( localFace, faces, 1.1, 2, CV_HAAR_SCALE_IMAGE, Size(m_facePos.height*m_minSearchScale, m_facePos.height*m_minSearchScale));
            if(faces.size()==1) {
                m_lostCount=0;
                m_state=NORMAL_FACE;
                emit face(m_facePos.x,m_facePos.y,m_facePos.width,m_facePos.height);
                qDebug()<<"normal face";
            } else {
                m_state=LOST_FACE;
                emit noface();
                qDebug()<<"lost face";
            }
        } else {
            emitDirection();
        }
    }
        break;
    case LOST_FACE:
    {
        Mat localFace;
        createRoi(image, localFace);
        face_cascade.detectMultiScale( localFace, faces, 1.1, 2, CV_HAAR_SCALE_IMAGE, Size(25, 25));
        if(faces.size()==1) {
            m_lostCount=0;
            m_facePos=faces[0];
            m_state=NORMAL_FACE;
            emit face(m_facePos.x,m_facePos.y,m_facePos.width,m_facePos.height);
            qDebug()<<"normal face";
        } else {
            Mat leftFace;
            rotate(localFace,m_rotate,leftFace);
            face_cascade.detectMultiScale( leftFace, faces, 1.1, 2, CV_HAAR_SCALE_IMAGE, Size(m_facePos.width*m_minSearchScale, m_facePos.height*m_minSearchScale));
            if(faces.size()==1) {
                m_lostCount=0;
                m_state=LEFT_FACE;
                qDebug()<<"left face";
                emitDirection();
            } else {
                Mat rightFace;
                rotate(localFace,-m_rotate,rightFace);
                face_cascade.detectMultiScale( rightFace, faces, 1.1, 2, CV_HAAR_SCALE_IMAGE, Size(m_facePos.width*m_minSearchScale, m_facePos.height*m_minSearchScale));
                if(faces.size()==1) {
                    m_lostCount=0;
                    m_state=RIGHT_FACE;
                    qDebug()<<"right face";
                    emitDirection();
                } else {
                    if(m_lostCount<4) {
                        m_lostCount++;
                    } else {
                        m_state=NO_FACE;
                        emit noface();
                        qDebug()<<"no face";
                    }
                }
            }
        }
        break;
    }
    }
}
