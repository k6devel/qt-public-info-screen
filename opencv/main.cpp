#include <QApplication>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <assert.h>

#include "camerawidget.h"
#include "camerawindow.h"

int main(int argc, char **argv) {
    QApplication app(argc, argv);

    CameraWindow *window = new CameraWindow();
    window->setWindowTitle("Qt + OpenCV example");
    window->show();

    int retval = app.exec();
    
    return retval;
}

