#include "camerawidget.h"
#include <QPainter>
#include <QPixmap>

using namespace cv;

// Constructor
CameraWidget::CameraWidget(FaceState *f, QWidget *parent)
    : QWidget(parent),
      penFace(Qt::SolidLine),
      penHand(Qt::SolidLine),
      m_faceState(f)
{
    connect(m_faceState,&FaceState::face,this,&CameraWidget::faceDetected);
    connect(m_faceState,&FaceState::noface,this,&CameraWidget::noFace);
    connect(m_faceState,&FaceState::left,this,&CameraWidget::leftFace);
    connect(m_faceState,&FaceState::right,this,&CameraWidget::rightFace);
    penFace.setColor(QColor(255,0,0,255));
    penHand.setColor(QColor(255,0,255,255));
    penFace.setWidth(3);
    penHand.setWidth(6);
    m_layout = new QVBoxLayout;
    m_imageLabel = new QLabel;

    QImage dummy(100, 100, QImage::Format_RGB32);
    m_image = dummy;

    m_layout->addWidget(m_imageLabel);

    for (int x = 0; x < 100; x ++)
        for (int y =0; y < 100; y++)
            m_image.setPixel(x,y,qRgb(x, y, y));

    m_imageLabel->setPixmap(QPixmap::fromImage(m_image));

    setLayout(m_layout);
}

CameraWidget::~CameraWidget(void)
{
}

void CameraWidget::putFrame(Mat *image)
{
    //Size s(320,240);
    Mat frame_gray;
    //cv::resize(*image,frame_gray,s);
    //the next two steps are needed otherwise format exceptions
    cvtColor( *image, frame_gray, CV_BGR2GRAY );
    frame_gray.convertTo(frame_gray, CV_8UC1);
    equalizeHist( frame_gray, frame_gray );
    QPixmap pm = Mat2QImage(frame_gray);
    QPainter p(&pm);
    m_faceState->doFrame(frame_gray,p);
    p.setPen(penFace);
    p.drawRect(m_facePos);
    m_imageLabel->setPixmap(pm);
    /*
    std::vector<Rect> faces;
    Mat frame_gray;
    cvtColor( *image, frame_gray, CV_BGR2GRAY );
    equalizeHist( frame_gray, frame_gray );
    face_cascade.detectMultiScale( frame_gray , faces, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30));

    QPixmap pm = Mat2QImage(frame_gray);
    QPainter p(&pm);
    for( size_t i = 0; i < faces.size(); i++ ) {
        Rect right(faces[i]);
        Rect left(faces[i]);

        //calculate bounds of hand sensitifity range
        right.x= right.x - right.width*2.5;
        right.y-= right.height;
        right.width *= 2;
        right.height *=2;
        if(right.x<0) {
            right.width -= -right.x;
            if(right.width<0) right.width=0;
            right.x=0;
        }
        if(right.y<0) {
            right.height+=right.y;
            right.y=0;
        }
        if(right.y+right.height>frame_gray.rows) {
            right.height = frame_gray.rows-right.y-1;
        }

        left.x= left.x + left.width*1.5;
        left.y-= left.height;
        left.width *= 2;
        left.height *= 2;
        if(left.y<0) {
            left.height+=left.y;
            left.y=0;
        }
        if(left.x>frame_gray.cols) { //out of window
            left.x=0;
            left.width=0;
        } else if((left.x+left.width)>frame_gray.cols) {
            left.width -= (left.x+left.width) - frame_gray.cols;
        }
        if(left.y+left.height>frame_gray.rows) {
            left.height = frame_gray.rows-left.y-1;
        }

        std::vector<Rect> rightHand;
        if(right.width) {
            Mat rightRoi = frame_gray(right);
            hand_cascade.detectMultiScale( rightRoi, rightHand, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30));
        }
        std::vector<Rect> leftHand;
        if(left.width) {
            Mat leftRoi = frame_gray(left);
            hand_cascade.detectMultiScale( leftRoi, leftHand, 1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(30, 30));
        }

        p.setPen(penFace);
        p.drawRect(QRect(faces[i].x,faces[i].y,faces[i].width,faces[i].height));
        p.drawRect(QRect(right.x,right.y,right.width,right.height));
        p.drawRect(QRect(left.x,left.y,left.width,left.height));
        p.setPen(penHand);
        for(size_t j=0; j<rightHand.size(); j++) {
                p.drawRect(QRect(right.x+rightHand[i].x,right.y+rightHand[i].y,rightHand[i].width,rightHand[i].height));
        }
        for(size_t j=0; j<leftHand.size(); j++) {
                p.drawRect(QRect(left.x+leftHand[i].x,left.y+leftHand[i].y,leftHand[i].width,leftHand[i].height));
        }
    }
    */
}

QPixmap CameraWidget::toPixmap(IplImage *cvimage) {
    int cvIndex, cvLineStart;

    switch (cvimage->depth) {
        case IPL_DEPTH_8U:
            switch (cvimage->nChannels) {
                case 3:
                    if ( (cvimage->width != m_image.width()) || (cvimage->height != m_image.height()) ) {
                        QImage temp(cvimage->width, cvimage->height, QImage::Format_RGB32);
                        m_image = temp;
                    }
                    cvIndex = 0; cvLineStart = 0;
                    for (int y = 0; y < cvimage->height; y++) {
                        unsigned char red,green,blue;
                        cvIndex = cvLineStart;
                        for (int x = 0; x < cvimage->width; x++) {
                            red = cvimage->imageData[cvIndex+2];
                            green = cvimage->imageData[cvIndex+1];
                            blue = cvimage->imageData[cvIndex+0];
                            
                            m_image.setPixel(x,y,qRgb(red, green, blue));
                            cvIndex += 3;
                        }
                        cvLineStart += cvimage->widthStep;                        
                    }
                    break;
                default:
                    qWarning("This number of channels is not supported\n");
                    break;
            }
            break;
        default:
            qWarning("This type of IplImage is not implemented in QOpenCVWidget\n");
            break;
    }

    return QPixmap::fromImage(m_image);
}


QPixmap CameraWidget::Mat2QImage(const Mat &src)
{
     cv::Mat temp(src.cols,src.rows,src.type());
     cv::cvtColor(src,temp,CV_GRAY2BGR);
     cv::cvtColor(temp,temp,CV_BGR2RGB);
     QImage dest= QImage((uchar*) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
     return QPixmap::fromImage(dest);
}

void CameraWidget::faceDetected(int x, int y, int width, int height)
{
    m_facePos = QRect(x, y ,width, height);
}

void CameraWidget::noFace()
{
   m_facePos = QRect(-1,-1,0,0);
}

void CameraWidget::leftFace()
{
    m_facePos = QRect(m_facePos.x()+m_facePos.width() ,m_facePos.y() , 0, m_facePos.height());
}

void CameraWidget::rightFace()
{
    m_facePos = QRect(m_facePos.x() , m_facePos.y() , 0, m_facePos.height());
}
