#ifndef IMAGEBUFFER_H
#define IMAGEBUFFER_H

#include "imagegrabber.h"

#include <QObject>
#include <QThread>
#include <QVector>
#include <QMutex>
#include <opencv/cv.h>
#include <opencv2/imgproc/imgproc.hpp>

class ImageBuffer:public QObject
{
    Q_OBJECT
public:
    ImageBuffer();
    cv::Mat* getImagePointer(bool grabber);
signals:
    void startThread();
protected:
    QVector<cv::Mat> m_images;
private:
    ImageGrabber m_imageGrabber;
    //stuff for getImage function
    QMutex m_mutex;
    int m_detectIdx;
    int m_grabberIdx;
    QThread m_grabberThread;
};

#endif // IMAGEBUFFER_H
