#include "imagegrabber.h"
#include "camerawindow.h"

ImageGrabber::ImageGrabber(ImageBuffer *ib):
    QObject(),
    m_ib(ib),
    cap(0)
{
    if(!cap.isOpened()) {
        qFatal("Failed to open video stream!");
    }
    cv::setNumThreads(3);
    cap.set(CV_CAP_PROP_FRAME_WIDTH,320);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT,240);
    qDebug()<<"video width :"<<cap.get(CV_CAP_PROP_FRAME_WIDTH);
    qDebug()<<"video height:"<<cap.get(CV_CAP_PROP_FRAME_HEIGHT);
}

void ImageGrabber::doWork() {
    for(;;) {
        cv::Mat *m = m_ib->getImagePointer(true);
        cap >> *m ;
    }
}
