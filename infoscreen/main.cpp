//#include "mainwindow.h"
#include <QQuickView>
#include <QQmlContext>
#include <QQmlEngine>
#include <QtQuick>
#include "unifeed.h"
#include "feedlist.h"
#include <QApplication>
#include "facedetector.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QThread::currentThread()->setPriority(QThread::HighPriority);
    QNetworkAccessManager manager(&a);
    UniFeed uniFeed(&manager, &a);

    FaceDetector fd(&a);
    //FIXME connect face detector events
    QQuickView view;
    qRegisterMetaType<FaceDetector*>();
    qmlRegisterType<FaceDetector>("FaceDetector",1,0,"FaceDetector");
    qRegisterMetaType<FeedEntry*>();
    qmlRegisterType<FeedEntry>("FeedEntry",1,0,"FeedEntry");
    view.setSource(QUrl::fromLocalFile("/home/main.qml"));
    QQmlContext *ctx = view.rootContext();
    FeedList fl(&a);
    uniFeed.setFeedUrl("http://leben-in-k6.de/blog/feed/");
    a.connect(&uniFeed,SIGNAL(newFeedEntry(FeedEntry*)),&fl,SLOT(newFeedEntry(FeedEntry*)));
    ctx->setContextProperty("feedList",&fl);
    ctx->setContextProperty("feedGetter",&uniFeed);
    ctx->setContextProperty("faceDetector",&fd);
    view.setWidth(500);
    view.setHeight(500);
    view.show();
    /*
    MainWindow w;
    w.connect(&w,SIGNAL(newUrl(QUrl)),&uniFeed,SLOT(setFeedUrl(QUrl)));
    w.show();
    */
    
    return a.exec();
}
