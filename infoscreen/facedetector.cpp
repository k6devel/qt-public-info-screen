#include <QPainter>
#include "facedetector.h"

FaceDetector::FaceDetector(QObject *parent) : ImageBuffer(),
    m_grabberThread(this),
    m_grabberIdx(0),
    m_detectIdx(1),
    m_faceState(new FaceState())
{
    //connect(button, SIGNAL(pressed()), this, SLOT(savePicture()));
    //connect(this, &FaceDetector::startThread, m_imageGrabber, &ImageGrabber::doWork);
    connect(m_faceState,SIGNAL(face(int,int,int,int)),SLOT(normalFace()));
    connect(m_faceState,SIGNAL(left()),this,SLOT(emitLeft()));
    connect(m_faceState,SIGNAL(right()),this,SLOT(emitRight()));
    startTimer(100);
}

void FaceDetector::putFrame(Mat *image)
{
    //Size s(320,240);
    Mat frame_gray;
    //cv::resize(*image,frame_gray,s);
    //the next two steps are needed otherwise format exceptions
    cvtColor( *image, frame_gray, CV_BGR2GRAY );
    frame_gray.convertTo(frame_gray, CV_8UC1);
    equalizeHist( frame_gray, frame_gray );
    //FIXME dirty
    QPainter p;
    m_faceState->doFrame(frame_gray,p);
}

//Puts a new frame in the widget every 100 msec
void FaceDetector::timerEvent(QTimerEvent*)
{
    Mat *m = getImagePointer(false);
    putFrame(m);
}
