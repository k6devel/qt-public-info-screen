#ifndef FACEDETECTOR_H
#define FACEDETECTOR_H

#include <QObject>
#include <QThread>
#include <QVector>
#include <QMutex>
#include <QWidget>
#include <QVBoxLayout>
#include <QDebug>
#include <QPushButton>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "../opencv/camerawidget.h"
#include "../opencv/imagegrabber.h"
#include "../opencv/imagebuffer.h"

class FaceDetector : public ImageBuffer
{
    Q_OBJECT
    Q_PROPERTY(QString direction READ direction NOTIFY directionChanged)
public:
    explicit FaceDetector(QObject *parent = 0);
    QString direction() const
    {
        return m_direction;
    }

private:
    void putFrame(Mat *image);
    CameraWidget *m_cvwidget;

    int m_photoCounter;
    QThread m_grabberThread;
    //stuff for getImage function
    QMutex m_mutex;
    int m_grabberIdx;
    int m_detectIdx;
    QString m_direction;

public slots:
    void normalFace() {emit directionChanged(m_direction=QString("normal"));}
    void emitLeft() {emit directionChanged(m_direction=QString("left"));}
    void emitRight() {emit directionChanged(m_direction=QString("right"));}
signals:
    void startThread();
    void directionChanged(QString direction);

protected:
    void timerEvent(QTimerEvent*);
    FaceState *m_faceState;
public slots:
};

#endif // FACEDETECTOR_H
